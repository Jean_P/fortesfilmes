require 'rails_helper'

RSpec.describe User, type: :model do
  context "User model Validations" do	
  	
	  	subject { 
	  	  described_class.new(name: 'Someone', phone: '9999-9999', address: 'Something Str. 00',
	  	  email: 'something@some.one', password: '999999', password_confirmation: '999999') 
	  	}
	    
    it "is valid with valid attributes" do    
      expect(subject).to be_valid
    end

    it "is not valid without a name" do
      subject.name = nil
      expect(subject).not_to be_valid
    end

    it "is not valid without a phone" do
      subject.phone = nil
      expect(subject).not_to be_valid
    end

    it "is not valid with an invalid phone" do
      subject.phone = 9999999999999999
      expect(subject).not_to be_valid
    end
    
    it "is valid without an address" do
      subject.address = nil
      expect(subject).to be_valid
    end
    
    it "is not valid without an email" do
      subject.email = nil
      expect(subject).not_to be_valid
    end

    it "is not valid with an invalid email" do
      subject.email = "emailnotevalid.com"
      expect(subject).not_to be_valid
    end
    
    it "is not valid without a password" do
      subject.password = nil
      expect(subject).not_to be_valid
    end  

  end
end
