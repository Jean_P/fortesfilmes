require 'rails_helper'

RSpec.describe Movie, type: :model do
  context "Movie model Validations" do	
  	
	  	subject { 
	  	  described_class.new(
    title: Faker::Book.title,
    genre: Faker::Book.genre,
    running_time: Faker::Number.within(range: 1..300),
    synopsis: Faker::Lorem.paragraph(sentence_count: 5),
    direction: Faker::Book.author
  	) 
	  	}
	    
    it "is valid with valid attributes" do    
      expect(subject).to be_valid
    end

    it "is not valid without a title" do
      subject.title = nil
      expect(subject).not_to be_valid
    end

    it "is valid without a running time" do
      subject.running_time = nil
      expect(subject).to be_valid
    end
    
    it "is not valid without a genre" do
      subject.genre = nil
      expect(subject).not_to be_valid
    end
    
    it "is not valid without a synopsis" do
      subject.synopsis = nil
      expect(subject).not_to be_valid
    end
    
    it "is not valid without a direction" do
      subject.direction = nil
      expect(subject).not_to be_valid
    end  

  end
end
