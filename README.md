# Fortes Filmes

Olá e obrigado de antemão pela oportunidade de participar desta seleção.
Criei um sistema simples na tentativa de atender aos requisitos mínimos e os adcionais. Houve limitação
relativo a sofisticação dos testes, da implementação das views mas na tentativa de ser mais sucinto.

## Sistema de Reserva de Filmes

O Sistema criado é de reserva de Filmes , atendendo ao que foi requisitado, permitindo escolher um filme por um usuário,
reservar uma data (se a o filme estiver disponível para desejada data e caso não haja uma reserva identica já realizada pelo usuário). Há uns testes básicos relativos as validações dos modelos e um sistema simples de usuários, mais para associar
 o usuário ao filme escolhido.

## O Que foi utilizado

Para desenvolver, Utilizei Ruby on Rails e as seguintes gems:
* PG : Foi desenvolvido usando  Postgresql como banco de dados
* Bootstrap + Datetimepicker : para melhorar o frontend, ajuda a estilizar datas também.
* Faker: Facilitar na criação dos seeds e Testes. Usei 
* RSPec: Utilizado neste sistema apenas para validação de modelos, mas excelente para testar de views a controllers
* Devise: Acelerar a criação de camada de usuários, bem mais prático que montar manuaalmente Sessions etc.
