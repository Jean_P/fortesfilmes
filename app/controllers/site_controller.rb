class SiteController < ApplicationController
  before_action :authenticate_user!, :except => [:index, :search, :genre]

  def index
    @movies = Movie.all
    @genres = @movies.map { |movie| movie.genre}.uniq
    @releases =  Movie.all.order("created_at DESC")
    @wanted =  Movie.all.order("booking_count DESC")
    
  end

  def release
  	@releases =  Movie.all.order("created_at DESC")
  end

  def wanted
    @wanted =  Movie.all.order("booking_count DESC")
  end

  def getbusca
    if params[:q]
      @results = Movie.where('title ilike ?', "%#{params[:q]}%")
      if !@results.empty?
        @results
        @msg = "Não encontramos nenhum resultado na busca por ' #{params[:q]} '  "
       
      else
        @results
        @msg = "Resultado da busca por ' #{params[:q]} ' : " 
      end  
    else
      @results = Movie.all
    end
  end

  def genre
    @genre = Movie.where(genre: params[:genre])
  end

  def book
    @booking_movie = Movie.find(params[:id])
  end

  private
  
    # Never trust parameters from the scary internet, only allow the white list through.
    def movie_params
      params.permit(:id)
    end

    def search_params
      params.permit(:q, :commit)
    end

end
