class BookingController < ApplicationController
 
 before_action :authenticate_user! 
 
  def show
    @bookings = MovieUser.where(user_id: current_user.id)
  end

 
  def new
    @movie_user = MovieUser.new
  end

  def edit
  end

  
  def create
    @movie_user = MovieUser.new(booking_params)

    respond_to do |format|
      if @movie_user.already_booked?
        format.html { redirect_to '/booking/show', alert: 'Você já fez essa reserva' }
      elsif @movie_user.is_available? 
        format.html { redirect_to "/book/#{@movie_user.movie_id}", alert: 'Escolha uma data válida' } if !@movie_user.valid_date?
        @movie_user.movie.booking_count += 1
        @movie_user.movie.save!
        @movie_user.save!
        format.html { redirect_to '/booking/show', notice: 'Sua reserva foi feita com sucesso' }
      else
        format.html { redirect_to '/booking/show', alert: 'Não é possível para essas datas escolhidas' }
       end
    end
  end

  private
    
    # Never trust parameters from the scary internet, only allow the white list through.
    def booking_params
      params.permit(:movie_id, :user_id, :start_booking, :end_booking)
    end

end
