class MovieUser < ApplicationRecord
  belongs_to :movie
  belongs_to :user

  validates :start_booking, presence: true
  validates :end_booking, presence: true

  def is_available?
  	start_date = self.start_booking
    end_date = self.end_booking

    start_taken = MovieUser.where(movie_id: self.movie_id, :start_booking => start_date..end_date)
    end_taken = MovieUser.where(movie_id: self.movie_id, :end_booking => start_date..end_date)

    if start_taken.empty? && end_taken.empty? 
      return true 
    else
      return false
    end

  end

  def already_booked?
    bookings = MovieUser.where(movie_id: self.movie_id, user_id: self.user_id, start_booking: self.start_booking, end_booking: self.end_booking)
    
    if bookings.count >= 1 
       return true 
    else
      return false
    end
  end

  def valid_date?
    if (self.start_booking >= self.end_booking ) || (self.start_booking < Date.today) 
      return false
    else
      return true
    end
  end

end
