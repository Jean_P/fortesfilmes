class Movie < ApplicationRecord

  has_many :users, through: :movie_users
 
  validates :title, presence: true
  validates :genre, presence: true
  validates :synopsis, presence: true
  validates :direction, presence: true

  def status
    taken = MovieUser.where(movie_id: self.id, start_booking: Date.today ).or( MovieUser.where(movie_id: self.id, end_booking: Date.today ) )    
    return 'Disponível' if taken.empty? 
    return 'Reservado' if !taken.empty? 
  end

  def self.search(search)
    if search
      where(:all, :conditions => ['title LIKE ?',  "%#{search}%"])
    else
       all
    end
  end

end