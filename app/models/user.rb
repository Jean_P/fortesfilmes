class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :movies, through: :movie_users

  validates :name, presence: true, length: {maximum: 50}
  validates :password, presence: true, length: {minimum: 6}
  VALID_EMAIL_FORMAT= /\S+@\w+\.\w{2,6}(\.\w{2})?/
  validates :email, presence: true, length: {maximum: 260}, format: { with: VALID_EMAIL_FORMAT}, uniqueness: {case_sensitive: false}
  VALID_PHONE_FORMAT= /(\(\d{2}\)\s?)?\d{4,5}-\d{4}/
  validates :phone, presence: true, format: {with: VALID_PHONE_FORMAT}
  before_save { self.email = email.downcase }
  
end
