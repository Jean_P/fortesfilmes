json.extract! movie, :id, :title, :genre, :running_time, :synopsis, :section, :direction, :cast, :status, :created_at, :updated_at
json.url movie_url(movie, format: :json)
