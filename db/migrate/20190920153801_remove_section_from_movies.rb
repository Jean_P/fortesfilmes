class RemoveSectionFromMovies < ActiveRecord::Migration[5.2]
  def change
    remove_column :movies, :section, :string
  end
end
