class AddBookingCountToMovies < ActiveRecord::Migration[5.2]
  def change
    add_column :movies, :booking_count, :integer, default: 0
    Movie.update_all(booking_count: 0)
  end
end
