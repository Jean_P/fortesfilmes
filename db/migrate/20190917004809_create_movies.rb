class CreateMovies < ActiveRecord::Migration[5.2]
  def change
    create_table :movies do |t|
      t.string :title
      t.string :genre
      t.integer :running_time
      t.string :synopsis
      t.string :section
      t.string :direction
      t.string :cast
      t.string :status

      t.timestamps
    end
  end
end
