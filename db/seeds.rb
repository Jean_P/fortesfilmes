# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
20.times do 
  r = rand(3)+1
  c = []
  r.times do 
    c << Faker::Artist.name
  end 


  Movie.create(
    title: Faker::Book.title,
    genre: Faker::Book.genre,
    running_time: Faker::Number.within(range: 1..300),
    synopsis: Faker::Lorem.paragraph(sentence_count: 5),
    direction: Faker::Book.author,
    cast: c   
  	)	

end

User.create(name: 'Admin', email: 'admin@admin.com' password: 123456, phone: '9999-9999', )