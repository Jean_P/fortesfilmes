Rails.application.routes.draw do
  devise_for :users
  resources :users, only: [:show, :edit, :update]
  root  to: 'site#index'
  resources :movies

  
  get '/site/genre/:genre', to: 'site#genre'
  get '/site/releases', to: 'site#release'
  get '/site/wanted', to: 'site#wanted'
  get '/search', to: 'site#search'
  get '/book/:id', to: 'site#book'
  post '/booking',  to: 'booking#create'
  get '/booking/show', to: 'booking#show'
  #post '/search', to: 'site#search'

  
end
